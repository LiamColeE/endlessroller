﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    Rigidbody rb;

	// Use this for initialization
	void Start ()
    {
        rb = this.gameObject.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.touchCount == 1)
        {
            Move(Input.GetTouch(0).deltaPosition);
        }
	}

    private void Move(Vector2 moveDir)
    {
        rb.AddForce(moveDir.x * 100, 0, moveDir.y * 100);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "DestroyPlayer")
        {
            Debug.Log("GameOver");
        }
    }
}
