﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour {

    public GameObject[] Obstacles;

	// Use this for initialization
	void Start () {
        Spawn();
	}

    void Spawn()
    {
        //Add Random functionality(Time Range, and differnt obstacle gameobjects)
        Instantiate(Obstacles[0],this.transform);
        Invoke("Spawn", 3);
    }
}
