﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
        //increase speed over time
        //add point system for each obstacle past
        this.transform.Translate(0, 0, -3f * Time.deltaTime);

        if (this.transform.childCount <= 0 )
        {
            Destroy(this.gameObject);
        }
	}
}
